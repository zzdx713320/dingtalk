from . import res_config_settings
from . import hr_department
from . import hr_employee
from . import dingtalk_config
from . import send_message
from . import res_users