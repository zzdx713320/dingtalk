from odoo import api, fields, models
from odoo.tools import config
import requests, json
from odoo.exceptions import ValidationError


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    def dingtalk(self):
        return self.env['dingtalk.base'].sudo()

    def get_user_list(self):
        """获取员工列表
        deptid：部门钉钉id
        """
        ding_env = self.dingtalk()
        access_token = ding_env.access_token()
        dept_ids = self.env['hr.department'].sudo().current_departments()
        users = ding_env.user_list(dept_ids=list(dept_ids.keys()), size=10, cursor=0, access_token=access_token)
        self.env['hr.employee'].add_employee(users)
        self.env['res.users'].add_user(users)
        self.env['res.users'].update_user(users)
        return True

    def get_departments(self):
        """获取部门"""
        ding_env = self.dingtalk()
        access_token = ding_env.access_token()
        dept_ids = ding_env.department_list(dept_ids=[1], access_token=access_token)
        self.env['hr.department'].sudo().add_department_list(dept_ids)
        # self.env['hr.department'].sudo().update_department_list(dept_ids)
        return True

    def sent_message(self):
        """发送工作通知"""
        pass
