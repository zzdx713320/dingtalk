# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'ODOO集成钉钉',
    'version': '1.0',
    'summary': 'ODOO系统集成钉钉',
    'sequence': 10,
    'description': """ODOO系统集成钉钉""",
    'author': "Bian Jing, bianjing166@163.com",
    'depends': ['base', 'hr'],
    'images': ['static/description/icon.png'],
    'data': [
        # 权限组
        'security/ir.model.access.csv',
        # 视图
        'views/dingtalk_config_view.xml',
        'views/send_message_view.xml',
        'views/send_media_view.xml',
        'views/send_log_view.xml',
        'views/res_config_settings_view.xml',
        'views/menus_view.xml'
    ],
    'application': True,
}
