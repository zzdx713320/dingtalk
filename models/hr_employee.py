from odoo import api, models, fields


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    dingtalk_id = fields.Char(string='钉钉id')

    def current_employee(self):
        objs = self.sudo().search([])
        return {obj.dingtalk_id: obj for obj in objs if obj.dingtalk_id}

    def add_employee(self, users):
        current_users = self.current_employee()
        for u in users:
            userid = str(u['userid'])
            if userid in current_users.keys(): continue
            self.create({'name': u['name'], 'dingtalk_id': userid})
